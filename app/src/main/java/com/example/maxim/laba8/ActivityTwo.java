package com.example.maxim.laba8;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.TextView;

public class ActivityTwo extends AppCompatActivity {

    public int p,g, y;
    public double bigX, bigY;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);

        Intent intent = getIntent();
        p = intent.getIntExtra("p", 0);
        g = intent.getIntExtra("g", 0);
        bigX = intent.getDoubleExtra("bigX", 0);
        y = 3;
        bigY = Math.round(Math.pow(g,y)%p);
        int k = ((int)Math.pow(bigX,y))%p;
        TextView ownView = (TextView) findViewById(R.id.p);
        ownView.setText((ownView.getText().toString()+String.valueOf(p)));
        TextView ownView1 = (TextView) findViewById(R.id.g);
        ownView1.setText((ownView1.getText().toString()+String.valueOf(g)));
        TextView ownView2 = (TextView) findViewById(R.id.y);
        ownView2.setText((ownView2.getText().toString()+String.valueOf(y)));
        TextView ownView3 = (TextView) findViewById(R.id.bigX);
        ownView3.setText((ownView3.getText().toString()+String.valueOf(bigX)));
        TextView ownView4 = (TextView) findViewById(R.id.bigY);
        ownView4.setText((ownView4.getText().toString()+String.valueOf(bigY)));
        TextView ownView5 = (TextView) findViewById(R.id.k);
        ownView5.setText((ownView5.getText().toString()+String.valueOf(k)));
    }

    public void onClick(View view){

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("bigY", bigY);
        startActivity(intent);
        finish();

    }

    public int rand(){
        int a = 1;
        int b = 10;
        int random_number = a + (int) (Math.random() * b);
        return random_number;
    }
}
