package com.example.maxim.laba8;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.*;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public int p, g, x;
    public double bigX, bigY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        p = 23;//generatePrime();
        g = 11;
        x = 5;//rand();
        bigX = Math.round((Math.pow(g,x))%p);

        TextView ownView = (TextView) findViewById(R.id.p);
        ownView.setText((ownView.getText().toString()+String.valueOf(p)));
        TextView ownView1 = (TextView) findViewById(R.id.g);
        ownView1.setText((ownView1.getText().toString()+String.valueOf(g)));
        TextView ownView2 = (TextView) findViewById(R.id.x);
        ownView2.setText((ownView2.getText().toString()+String.valueOf(x)));
        TextView ownView3 = (TextView) findViewById(R.id.bigX);
        ownView3.setText((ownView3.getText().toString()+String.valueOf(bigX)));

    }

    @Override
    protected void onResume(){
        super.onResume();
        Intent intent = getIntent();
        bigY = intent.getDoubleExtra("bigY", 0);
        TextView ownView = (TextView) findViewById(R.id.bigY);
        ownView.setText((ownView.getText().toString()+String.valueOf(bigY)));
        int k = ((int)Math.pow(bigY,x))%p;
        TextView ownView1 = (TextView) findViewById(R.id.k);
        ownView1.setText((ownView1.getText().toString()+String.valueOf(k)));
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.create:
                create();
                break;
        }
    }

    public void create(){
        double bigX = Math.round((Math.pow(g,x))%p);

        Intent intent = new Intent(this, ActivityTwo.class);
        intent.putExtra("p", p);
        intent.putExtra("g", g);
        intent.putExtra("bigX", bigX);
        startActivity(intent);
        finish();
    }

    public boolean isPrime(int num) {
        if (num == 2)
            return true;
        if (num < 2 || num % 2 == 0)
            return false;
        for (int i = 3; i * i <= num; i += 2)
            if (num % i == 0)
                return false;
        return true;
    }

    public int rand(){
        int a = 1;
        int b = 10;
        int random_number = a + (int) (Math.random() * b);
        return random_number;
    }

    public int generatePrime(){
        int a = 2;
        int b = 100;
        int random_number = 0;
        while(!isPrime(random_number)){
            random_number = a + (int) (Math.random() * b);
        }
        return random_number;
    }

    public int findTheReverse(int num, int p){
        int reverse = 0;
        for(int i = 1; i<p;i++)
        {
            if((num*i)%p==1){
                reverse=i;
                break;
            }
        }
        return reverse;
    }
}
